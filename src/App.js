
import './App.css';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import GridComponent from './components//CodeGenGridComponent';
import FormComponent from './components/CodegenForm';
import Header from './components/HeaderComponent';

function App() {
  return (
    <Router>
      <Header></Header>
      <Switch>

      <Route path="/form" component={FormComponent}></Route>
        
        <Route path="/" component={GridComponent}></Route>
       
      </Switch>
      
    </Router>
  );
}

export default App;
