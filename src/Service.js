import $ from 'jquery';

/**
* FC_PC_4
* GC_PC_41
* getCallGateway() by passing a parameter which URL 
* needs to be triggered and checking the callback data from the 
* service
* GC_PC_43
* Execute the callback function
*/
let getCallGateway = async (URL, callback)=>{

    try{
        
       //console.log("navaew",URL);
        
        let response = await $.ajax({
            type:"GET",
            url:URL
        });
        console.log(response);
        
        let result ={
            "success":true,
            "response":response
        }
       
        callback(result);
    }
    catch(ex){
       
        let result ={
            "success":false,
            "response":ex
        }
        callback(result);

    }
}

/**
* FC_PC_15 , FC_PC_26 , GC_PC_06
* getCallGateway() by passing a parameter which URL 
* needs to be triggered and data that which we are 
* going to save in the database, a call back which it sends
* response to the action.
* GC_PC_08
* Execute the callback function 
*/
let postCallGateway = async (URL, data, callback )=>{

    try{
        console.log((URL));
         console.log(data);        
        var response =await $.ajax({
            type:"POST",
            url:URL,
            data:data
        });
        console.log(response);  
        
        let result ={
            "success":true,
            "response":response
        }
       
        callback(result);
    }
    catch(ex){
      
        let result ={
            "success":false,
            "response":ex
        }
        callback(result);

    }
}
/**
* FC_PC_15
* getCallGateway() by passing a parameter which URL 
* needs to be triggered and data that which we are 
* going to save in the database, a call back which it sends
* response to the action.
*/
let putCallGateway = async (URL, data, callback )=>{

    try{
        console.log((URL));
         console.log(data);        
        var response =await $.ajax({
            type:"PUT",
            url:URL,
            data:data
        });
        
        
        let result ={
            "success":true,
            "response":response
        }
       
        callback(result);
    }
    catch(ex){
      
        let result ={
            "success":false,
            "response":ex
        }
        callback(result);

    }
}

/**
* GC_PC_19 
* getCallGateway() by passing a parameter which URL 
* needs to be triggered that which we are 
* going to save in the database, a call back which it sends
* response to the action.
* GC_PC_21
* Execute the callback function 
*/
let deleteCallGateway = async (URL, callback )=>{

    try{       
        var response = await $.ajax({
            type:"DELETE",
            url:URL
        });
        console.log(URL);
        
        let result = {
            "success":true,
            "response":response
        }
       
        callback(result);
    }
    catch(ex){
      
        let result = {
            "success":false,
            "response":ex
        }
        callback(result);

    }
}

export { getCallGateway, postCallGateway, putCallGateway, deleteCallGateway }