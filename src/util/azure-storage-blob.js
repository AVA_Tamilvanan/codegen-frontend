import dateformat from 'dateformat';

import { BlobServiceClient, ContainerClient} from '@azure/storage-blob';

// THIS IS SAMPLE CODE ONLY - DON'T STORE TOKEN IN PRODUCTION CODE
const sasToken = process.env.storagesastoken || "sv=2020-08-04&ss=b&srt=sco&sp=rwdlactfx&se=2031-10-08T02:47:34Z&st=2021-10-07T18:47:34Z&spr=https,http&sig=oPQAYBxF6chETtNqNuTu4sz8w0n9GslbVZy3%2BIjj1B4%3D"; // Fill string with your SAS token
const containerName = `ava-cdegen-inp-np-con`;
const storageAccountName = process.env.storageresourcename || "avaeuscdegennpstgacc"; // Fill string with your Storage resource name
// </snippet_package>

// <snippet_isStorageConfigured>
// Feature flag - disable storage feature to app if not configured
export const isStorageConfigured = () => {
  debugger;
  return (!storageAccountName || !sasToken) ? false : true;
}

// const getBlobsInContainer = async (containerClient) => {
//   debugger;
//   const returnedBlobUrls = [];

//   // get list of blobs in container
//   // eslint-disable-next-line
//   for await (const blob of containerClient.listBlobsFlat()) {
//     // if image is public, just construct URL
//     returnedBlobUrls.push(
//       `https://${storageAccountName}.blob.core.windows.net/${containerName}/${blob.name}`
//     );
//   }

//   return returnedBlobUrls;
// }
// </snippet_getBlobsInContainer>

// <snippet_createBlobInContainer>
const createBlobInContainer = async (containerClient, file) => {
  debugger
//   file.name = file.name.trim();
  console.log(file.name);
  let filename = dateformat(new Date(),"yyyymmddHHMMssl") +"-"+ file.name
  // create blobClient for container
  
    const blobClient = containerClient.getBlockBlobClient(filename);

//   file
  // set mimetype as determined from browser with file upload control
  const options = { blobHTTPHeaders: { blobContentType: file.type } };

  // upload file
 let loc= await blobClient.uploadBrowserData(file, options);

    return filename;
}
// </snippet_createBlobInContainer>

// <snippet_uploadFileToBlob>
const uploadFileToBlob = async (file) => {
  if (!file) return [];

  // get BlobService = notice `?` is pulled out of sasToken - if created in Azure portal
  const blobService = new BlobServiceClient(
    `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`
  );

  // get Container - full public read access
  const containerClient = blobService.getContainerClient(containerName);
  await containerClient.createIfNotExists({
    access: 'container',
  });

  // upload file
  let loc = await createBlobInContainer(containerClient, file);
 console.log(containerClient);
  // get list of blobs in container
  return loc;
};
// </snippet_uploadFileToBlob>

export default uploadFileToBlob;
// import { BlobServiceClient, ContainerClient} from '@azure/storage-blob';
// //const {​ v1: uuidv1}​ = require('uuid');
// const AZURE_STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=codegenstorage;AccountKey=ZS5tz2VzS1SRJT9wkpRV8EJsRG65liKNfUaAlhc0ssRnsYCaWrYENUSFpw0C32elVrc61lev8x42pV70Dm/3tQ==;EndpointSuffix=core.windows.net";
//  main=()=>{​}​
// const blobServiceClient = BlobServiceClient.fromConnectionString(AZURE_STORAGE_CONNECTION_STRING);
// //  main = async() => {​
// //   let i = 1;
// //   let containers = blobServiceClient.listContainers();
// //   for await (const container of containers) {​
// //     console.log(`Container ${​i++}​: ${​container.name}​`);
// //   }​
// }​
// main();
// // Create a unique name for the container
// // createcontainer();
// // async function createcontainer()
// // {​
// const containerName = "user-input";
// // console.log('\nCreating container...');
// // console.log('\t', containerName);
// // // Get a reference to a container
//  const containerClient = blobServiceClient.getContainerClient(containerName);
// // const createContainerResponse  = await containerClient.create();
// debugger;
// // // Create the container
// // //const createContainerResponse = await containerClient.create();
// // console.log("Container was created successfully. requestId: ", createContainerResponse.requestId);
// // console.log("Hi");
//   const content = "Hello world from lokesh!";
//   const blobName = "newblob" + new Date().getTime();
//   console.log(blobName);
//   const blockBlobClient = containerClient.getBlockBlobClient(blobName);
//   debugger;
// const uploadBlobResponse = blockBlobClient.upload(content, content.length);
//   console.log(`Upload block blob ${​blobName}​ successfully`, uploadBlobResponse.requestId);